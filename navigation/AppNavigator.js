import React from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  ImageBackground,
  Animated,
  BackHandler,
  StyleSheet,
} from 'react-native';
import Dialog from "react-native-dialog";

import MainMenuScreen from '../screens/MainMenu';
import PreferencesScreen from '../screens/Preferences';
import BudgetScreen from '../screens/MealsSelection/Budget';
import DayScreen from '../screens/MealsSelection/Day';
import MealScreen from '../screens/MealsSelection/Meal';
import RecipesScreen from '../screens/Recipes';
import IngredientsScreen from '../screens/Recipes/Ingredients';
import RecipeScreen from '../screens/Recipes/Recipe';
import ShoppingListScreen from '../screens/Shopping/List';
import ResumeSelectionScreen from '../screens/MealsSelection/Resume';
import NotFoundMealsScreen from '../screens/MealsSelection/NotFound';
import { previousScreen } from '../actions';

class ShopCookNavigator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Screens: {
        MainMenuScreen,
        PreferencesScreen,
        BudgetScreen,
        DayScreen,
        MealScreen,
        RecipesScreen,
        IngredientsScreen,
        RecipeScreen,
        ShoppingListScreen,
        ResumeSelectionScreen,
        NotFoundMealsScreen,
      },
      dialogOpen: true,
    }

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    const { dispatch } = this.props;

    dispatch(previousScreen());
    return true;  
  }

  render() {
    const { currentScreen } = this.props;
    const { Screens } = this.state;
    const Screen = Screens[currentScreen];
     
    if (new Date(2018, 10, 31) <= new Date()) {
      return (
        <ImageBackground
        style={styles.imageBackground} 
        source={require('../assets/images/splash.png')}
        >
          <Dialog.Container visible={this.state.dialogOpen}>
            <Dialog.Title>Test terminé</Dialog.Title>
            <Dialog.Description>Test terminé. Vous pouvez desinstaller l'application. Merci !</Dialog.Description>
            <Dialog.Button label="Ok" onPress={() => this.setState({dialogOpen: false, text: ""})} />
          </Dialog.Container>
        </ImageBackground>
      )
    }

    return (
      <ImageBackground
        style={styles.imageBackground} 
        source={currentScreen !== "MainMenuScreen" ? require('../assets/images/ui/fond_trans.png') : require('../assets/images/ui/fond.png')}
      > 
        <Screen />
      </ImageBackground>
    );
  }
}

function mapStateToProps({
  currentScreen,
}) {
  return {
    currentScreen,
  }
}


const styles = StyleSheet.create({
  imageBackground: {
    flex: 1, 
    width: '100%',  
    height: '100%',
  }
});

export default connect(mapStateToProps)(ShopCookNavigator);

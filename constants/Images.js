

const images = {
  'oignon.jpg': require('../assets/images/aliments/oignon.jpg'),
  'cacahute.jpg': require('../assets/images/aliments/cacahute.jpg'),
  'semoule.jpg': require('../assets/images/aliments/semoule.jpg'),
  'mozzarella.jpg': require('../assets/images/aliments/mozzarella.jpg'),
  'pommesdeterregrenailles.jpg': require('../assets/images/aliments/pommesdeterregrenailles.jpg'),
  'miedepain.jpg': require('../assets/images/aliments/miedepain.jpg'),
  'carotte.jpg': require('../assets/images/aliments/carotte.jpg'),
  'amandesenpoudre.jpg': require('../assets/images/aliments/amandesenpoudre.jpg'),
  'rosette.jpg': require('../assets/images/aliments/rosette.jpg'),
  'pommesdeterre.jpg': require('../assets/images/aliments/pommesdeterre.jpg'),
  'olives.jpg': require('../assets/images/aliments/olives.jpg'),
  'crevettes.jpg': require('../assets/images/aliments/crevettes.jpg'),
  'sucreenpoudre.jpg': require('../assets/images/aliments/sucreenpoudre.jpg'),
  'siropdrable.jpg': require('../assets/images/aliments/siropdrable.jpg'),
  'capres.jpg': require('../assets/images/aliments/capres.jpg'),
  'viandedegrison.jpg': require('../assets/images/aliments/viandedegrison.jpg'),
  'siropdagave.jpg': require('../assets/images/aliments/siropdagave.jpg'),
  'spaghetti.jpg': require('../assets/images/aliments/spaghetti.jpg'),
  'crmeliquide.jpg': require('../assets/images/aliments/crmeliquide.jpg'),
  'poitrinedeveauhach.jpg': require('../assets/images/aliments/poitrinedeveauhach.jpg'),
  'coquillettes.jpg': require('../assets/images/aliments/coquillettes.jpg'),
  'pte.jpg': require('../assets/images/aliments/pte.jpg'),
  'laitdecoco.jpg': require('../assets/images/aliments/laitdecoco.jpg'),
  'sucreroux.jpg': require('../assets/images/aliments/sucreroux.jpg'),
  'fromageraclette.jpg': require('../assets/images/aliments/fromageraclette.jpg'),
  'ciboulette.jpg': require('../assets/images/aliments/ciboulette.jpg'),
  'riz.jpg': require('../assets/images/aliments/riz.jpg'),
  'noixdemuscade.jpg': require('../assets/images/aliments/noixdemuscade.jpg'),
  'persil.jpg': require('../assets/images/aliments/persil.jpg'),
  'levureboulangresansgluten.jpg': require('../assets/images/aliments/levureboulangresansgluten.jpg'),
  'canelle.jpg': require('../assets/images/aliments/canelle.jpg'),
  'ravioles.jpg': require('../assets/images/aliments/ravioles.jpg'),
  'cloudegirofle.jpg': require('../assets/images/aliments/cloudegirofle.jpg'),
  'coulisdetomates.jpg': require('../assets/images/aliments/coulisdetomates.jpg'),
  'potiron.jpg': require('../assets/images/aliments/potiron.jpg'),
  'olivesvertes.jpg': require('../assets/images/aliments/olivesvertes.jpg'),
  'cpes.jpg': require('../assets/images/aliments/cpes.jpg'),
  'cresson.jpg': require('../assets/images/aliments/cresson.jpg'),
  'fenouil.jpg': require('../assets/images/aliments/fenouil.jpg'),
  'pignon.jpg': require('../assets/images/aliments/pignon.jpg'),
  'morilles.jpg': require('../assets/images/aliments/morilles.jpg'),
  'oseille.jpg': require('../assets/images/aliments/oseille.jpg'),
  'saucesoja.jpg': require('../assets/images/aliments/saucesoja.jpg'),
  'cumin.jpg': require('../assets/images/aliments/cumin.jpg'),
  'oeuf.jpg': require('../assets/images/aliments/oeuf.jpg'),
  'menthe.jpg': require('../assets/images/aliments/menthe.jpg'),
  'parmesan.jpg': require('../assets/images/aliments/parmesan.jpg'),
  'chouxkhale.jpg': require('../assets/images/aliments/chouxkhale.jpg'),
  'rotideporc.jpg': require('../assets/images/aliments/rotideporc.jpg'),
  'tofu.jpg': require('../assets/images/aliments/tofu.jpg'),
  'gruyreaop.jpg': require('../assets/images/aliments/gruyreaop.jpg'),
  'huiledecoco.jpg': require('../assets/images/aliments/huiledecoco.jpg'),
  'lait.jpg': require('../assets/images/aliments/lait.jpg'),
  'jambon.jpg': require('../assets/images/aliments/jambon.jpg'),
  'concombre.jpg': require('../assets/images/aliments/concombre.jpg'),
  'curcuma.jpg': require('../assets/images/aliments/curcuma.jpg'),
  'echalote.jpg': require('../assets/images/aliments/echalote.jpg'),
  'raisinssecs.jpg': require('../assets/images/aliments/raisinssecs.jpg'),
  'sucreenpoudre1.jpg': require('../assets/images/aliments/sucreenpoudre1.jpg'),
  'steackpoire.jpg': require('../assets/images/aliments/steackpoire.jpg'),
  'yaourtpais.jpg': require('../assets/images/aliments/yaourtpais.jpg'),
  'herbesdeprovence.jpg': require('../assets/images/aliments/herbesdeprovence.jpg'),
  'poireau.jpg': require('../assets/images/aliments/poireau.jpg'),
  'sel.jpg': require('../assets/images/aliments/sel.jpg'),
  'avocat.jpg': require('../assets/images/aliments/avocat.jpg'),
  'poulet.jpg': require('../assets/images/aliments/poulet.jpg'),
  'ctelettedagneau.jpg': require('../assets/images/aliments/ctelettedagneau.jpg'),
  'noix.jpg': require('../assets/images/aliments/noix.jpg'),
  'epinard.jpg': require('../assets/images/aliments/epinard.jpg'),
  'aubergine.jpg': require('../assets/images/aliments/aubergine.jpg'),
  'salade.jpg': require('../assets/images/aliments/salade.jpg'),
  'cornichon.jpg': require('../assets/images/aliments/cornichon.jpg'),
  'farine.jpg': require('../assets/images/aliments/farine.jpg'),
  'gruyrerap.jpg': require('../assets/images/aliments/gruyrerap.jpg'),
  'grainesdesesame.jpg': require('../assets/images/aliments/grainesdesesame.jpg'),
  'fromagedechvre.jpg': require('../assets/images/aliments/fromagedechvre.jpg'),
  'cheddar.jpg': require('../assets/images/aliments/cheddar.jpg'),
  'huilevgtale.jpg': require('../assets/images/aliments/huilevgtale.jpg'),
  'thym.jpg': require('../assets/images/aliments/thym.jpg'),
  'gingembre.jpg': require('../assets/images/aliments/gingembre.jpg'),
  'huilevegetale.jpg': require('../assets/images/aliments/huilevegetale.jpg'),
  'sesameengraine.jpg': require('../assets/images/aliments/sesameengraine.jpg'),
  'pomme.jpg': require('../assets/images/aliments/pomme.jpg'),
  'crozet.jpg': require('../assets/images/aliments/crozet.jpg'),
  'grainesdetournesol.jpg': require('../assets/images/aliments/grainesdetournesol.jpg'),
  'riz1.jpg': require('../assets/images/aliments/riz1.jpg'),
  'poivronrouge.jpg': require('../assets/images/aliments/poivronrouge.jpg'),
  'yaourtsoja.jpg': require('../assets/images/aliments/yaourtsoja.jpg'),
  'tomates.jpg': require('../assets/images/aliments/tomates.jpg'),
  'chouxrouge.jpg': require('../assets/images/aliments/chouxrouge.jpg'),
  'feta.jpg': require('../assets/images/aliments/feta.jpg'),
  'citronvert-lime.jpg': require('../assets/images/aliments/citronvert-lime.jpg'),
  'poixchiche.jpg': require('../assets/images/aliments/poixchiche.jpg'),
  'levureboulangre1.jpg': require('../assets/images/aliments/levureboulangre1.jpg'),
  'masengrain.jpg': require('../assets/images/aliments/masengrain.jpg'),
  'champignondeparis.jpg': require('../assets/images/aliments/champignondeparis.jpg'),
  'poivre.jpg': require('../assets/images/aliments/poivre.jpg'),
  'saumon.jpg': require('../assets/images/aliments/saumon.jpg'),
  'asperges.jpg': require('../assets/images/aliments/asperges.jpg'),
  'lardons.jpg': require('../assets/images/aliments/lardons.jpg'),
  'moutarde.jpg': require('../assets/images/aliments/moutarde.jpg'),
  'farinesansgluten.jpg': require('../assets/images/aliments/farinesansgluten.jpg'),
  'poussedpinard.jpg': require('../assets/images/aliments/poussedpinard.jpg'),
  'eau.jpg': require('../assets/images/aliments/eau.jpg'),
  'painsansgluten.jpg': require('../assets/images/aliments/painsansgluten.jpg'),
  'beaufort.jpg': require('../assets/images/aliments/beaufort.jpg'),
  'lardons1.jpg': require('../assets/images/aliments/lardons1.jpg'),
  'cbette.jpg': require('../assets/images/aliments/cbette.jpg'),
  'petitspois.jpg': require('../assets/images/aliments/petitspois.jpg'),
  'vinaigrederiz.jpg': require('../assets/images/aliments/vinaigrederiz.jpg'),
  'vinaigredecidre.jpg': require('../assets/images/aliments/vinaigredecidre.jpg'),
  'coriandre1.jpg': require('../assets/images/aliments/coriandre1.jpg'),
  'chouxchinois.jpg': require('../assets/images/aliments/chouxchinois.jpg'),
  'ptesansgluten.jpg': require('../assets/images/aliments/ptesansgluten.jpg'),
  'courgette.jpg': require('../assets/images/aliments/courgette.jpg'),
  'huiled-olive.jpg': require('../assets/images/aliments/huiled-olive.jpg'),
  'nuocmam.jpg': require('../assets/images/aliments/nuocmam.jpg'),
  'jamboncru.jpg': require('../assets/images/aliments/jamboncru.jpg'),
  'huiledessame.jpg': require('../assets/images/aliments/huiledessame.jpg'),
  'citron.jpg': require('../assets/images/aliments/citron.jpg'),
  'basilic.jpg': require('../assets/images/aliments/basilic.jpg'),
  'bacon.jpg': require('../assets/images/aliments/bacon.jpg'),
  'boulgour.jpg': require('../assets/images/aliments/boulgour.jpg'),
  'betterave.jpg': require('../assets/images/aliments/betterave.jpg'),
  'jambon.jpeg': require('../assets/images/aliments/jambon.jpeg'),
  'quinoanoir.jpg': require('../assets/images/aliments/quinoanoir.jpg'),
  'beurresal.jpg': require('../assets/images/aliments/beurresal.jpg'),
  'bouilloncubevegetal.jpg': require('../assets/images/aliments/bouilloncubevegetal.jpg'),
  'pimentenpaillette.jpg': require('../assets/images/aliments/pimentenpaillette.jpg'),
  'paprika.jpg': require('../assets/images/aliments/paprika.jpg'),
  'ail.jpg': require('../assets/images/aliments/ail.jpg'),
  'lentilles.jpg': require('../assets/images/aliments/lentilles.jpg'),
  'levureboulangre.jpg': require('../assets/images/aliments/levureboulangre.jpg'),
  'quenelles.jpg': require('../assets/images/aliments/quenelles.jpg'),
  'poussesdesoja.jpg': require('../assets/images/aliments/poussesdesoja.jpg'),
  'pommedeterre.jpg': require('../assets/images/aliments/pommedeterre.jpg'),
  'beurre.jpg': require('../assets/images/aliments/beurre.jpg'),
  'gingembrepoudre.jpg': require('../assets/images/aliments/gingembrepoudre.jpg'),
  'oignonrouge.jpg': require('../assets/images/aliments/oignonrouge.jpg'),
  'radisnoir.jpg': require('../assets/images/aliments/radisnoir.jpg'),
  'paindemie.jpg': require('../assets/images/aliments/paindemie.jpg'),
  'noodles.jpg': require('../assets/images/aliments/noodles.jpg'),
  'escalopedepoulet.jpg': require('../assets/images/aliments/escalopedepoulet.jpg'),
  'vinaigredevin.jpg': require('../assets/images/aliments/vinaigredevin.jpg'),
  'brocolis.jpg': require('../assets/images/aliments/brocolis.jpg'),
  'vinblanc.jpg': require('../assets/images/aliments/vinblanc.jpg'),
  'vacherinfribourgeois.jpg': require('../assets/images/aliments/vacherinfribourgeois.jpg'),
  'pruneaux.jpg': require('../assets/images/aliments/pruneaux.jpg'),
  'boeufhach.jpg': require('../assets/images/aliments/boeufhach.jpg'),
  'saladeverte.jpg': require('../assets/images/aliments/saladeverte.jpg'),
  'ricotta.jpg': require('../assets/images/aliments/ricotta.jpg'),
  'quinoa.jpg': require('../assets/images/aliments/quinoa.jpg'),
  'coriandre.jpg': require('../assets/images/aliments/coriandre.jpg'),
  'vinaigrebalsamique.jpg': require('../assets/images/aliments/vinaigrebalsamique.jpg'),
  'mas.jpg': require('../assets/images/aliments/mas.jpg'),
  'feuillesdebrique.jpg': require('../assets/images/aliments/feuillesdebrique.jpg'),
  'paindemiesansgluten.jpg': require('../assets/images/aliments/paindemiesansgluten.jpg'),
  'miel.jpg': require('../assets/images/aliments/miel.jpg'),
  'jambonetpuree.jpeg': require('../assets/images/aliments/jambonetpuree.jpeg'),
  'oeuflacoque.jpg': require('../assets/images/aliments/oeuflacoque.jpg'),
  'boulettesviande.jpg': require('../assets/images/aliments/boulettesviande.jpg'),
  'saladetomatesmozzarella.jpg': require('../assets/images/aliments/saladetomatesmozzarella.jpg'),
  'spaghetti-arrabiati.jpg': require('../assets/images/aliments/spaghetti-arrabiati.jpg'),
  'raclettesavoyarde.jpg': require('../assets/images/aliments/raclettesavoyarde.jpg'),
  'raviolescrmeetgruyre.jpg': require('../assets/images/aliments/raviolescrmeetgruyre.jpg'),
  'saladedepoischiche.jpg': require('../assets/images/aliments/saladedepoischiche.jpg'),
  'cotelettesagneauetsemoule.jpg': require('../assets/images/aliments/cotelettesagneauetsemoule.jpg'),
  'soupepoireau.jpg': require('../assets/images/aliments/soupepoireau.jpg'),
  'papillotesaumonravioles.jpg': require('../assets/images/aliments/papillotesaumonravioles.jpg'),
  'maisetsemoule.jpg': require('../assets/images/aliments/maisetsemoule.jpg'),
  'croquechevrechouxkhale.jpg': require('../assets/images/aliments/croquechevrechouxkhale.jpg'),
  'pommes-terre-sautees-ailetlard.jpg': require('../assets/images/aliments/pommes-terre-sautees-ailetlard.jpg'),
  'saladedelentilles.jpg': require('../assets/images/aliments/saladedelentilles.jpg'),
  'coquillettejamboncuit.jpg': require('../assets/images/aliments/coquillettejamboncuit.jpg'),
  'croquemonsieur.jpg': require('../assets/images/aliments/croquemonsieur.jpg'),
  'coquillettejambonblanc.jpg': require('../assets/images/aliments/coquillettejambonblanc.jpg'),
  'ptepesto.jpg': require('../assets/images/aliments/ptepesto.jpg'),
  'soupecourgettes.jpg': require('../assets/images/aliments/soupecourgettes.jpg'),
  'buddhabowl.jpg': require('../assets/images/aliments/buddhabowl.jpg'),
  '9.jpg': require('../assets/images/aliments/9.jpg'),
  'feuilletlafeta.jpg': require('../assets/images/aliments/feuilletlafeta.jpg'),
  'soupedecourgettes.jpg': require('../assets/images/aliments/soupedecourgettes.jpg'),
  'padtha.jpg': require('../assets/images/aliments/padtha.jpg'),
  'courgettesfarcies.jpg': require('../assets/images/aliments/courgettesfarcies.jpg'),
  'croqueauchevreetbacon.jpg': require('../assets/images/aliments/croqueauchevreetbacon.jpg'),
  'croteaufromage.jpg': require('../assets/images/aliments/croteaufromage.jpg'),
  'poireauxvinaigrette.jpg': require('../assets/images/aliments/poireauxvinaigrette.jpg'),
  'moussakagrecque.jpg': require('../assets/images/aliments/moussakagrecque.jpg'),
  'auberginefarcies.jpg': require('../assets/images/aliments/auberginefarcies.jpg'),
  'veloutdecourge.jpg': require('../assets/images/aliments/veloutdecourge.jpg'),
  'tofubrouillauxasperges.jpg': require('../assets/images/aliments/tofubrouillauxasperges.jpg'),
  'masaubeurre.jpg': require('../assets/images/aliments/masaubeurre.jpg'),
  'saladebowltofukhale.jpg': require('../assets/images/aliments/saladebowltofukhale.jpg'),
  'quenellesbechamel.png': require('../assets/images/aliments/quenellesbechamel.png'),
  'poischiche.jpg': require('../assets/images/aliments/poischiche.jpg'),
  '12.jpg': require('../assets/images/aliments/12.jpg'),
  'oeufcocottecpes.jpg': require('../assets/images/aliments/oeufcocottecpes.jpg'),
  'poireauxlavinaigrette.jpg': require('../assets/images/aliments/poireauxlavinaigrette.jpg'),
  'csoupedecresson.jpg': require('../assets/images/aliments/csoupedecresson.jpg'),
  'episdemas.jpg': require('../assets/images/aliments/episdemas.jpg'),
  'raclette.jpg': require('../assets/images/aliments/raclette.jpg'),
  'moussaka.jpg': require('../assets/images/aliments/moussaka.jpg'),
  '11.jpg': require('../assets/images/aliments/11.jpg'),
  'saladedefenouiletradisnoir.jpg': require('../assets/images/aliments/saladedefenouiletradisnoir.jpg'),
  'tofusautasiatique.jpg': require('../assets/images/aliments/tofusautasiatique.jpg'),
  'escalopepouletbrocolis.jpg': require('../assets/images/aliments/escalopepouletbrocolis.jpg'),
  'hachisparmentier.jpg': require('../assets/images/aliments/hachisparmentier.jpg'),
  'salademledautomne.jpg': require('../assets/images/aliments/salademledautomne.jpg'),
  'tianauxlgumes.jpg': require('../assets/images/aliments/tianauxlgumes.jpg'),
  'crotefromage.jpg': require('../assets/images/aliments/crotefromage.jpg'),
  'saladefenouiletradisnoir.jpg': require('../assets/images/aliments/saladefenouiletradisnoir.jpg'),
  'crozetlasavoyarde.jpg': require('../assets/images/aliments/crozetlasavoyarde.jpg'),
  '10.jpg': require('../assets/images/aliments/10.jpg'),
  'salademleautomne.jpg': require('../assets/images/aliments/salademleautomne.jpg'),
  'epismaisetsteack.jpg': require('../assets/images/aliments/epismaisetsteack.jpg'),
  'rizetpoischiche.jpg': require('../assets/images/aliments/rizetpoischiche.jpg'),
  'steaketpommesdeterresautes.jpg': require('../assets/images/aliments/steaketpommesdeterresautes.jpg'),
  'crozetsavoyard.jpg': require('../assets/images/aliments/crozetsavoyard.jpg'),
  'saladebowltofu.jpg': require('../assets/images/aliments/saladebowltofu.jpg'),
  'papillote-de-saumon-aux-ravioles.jpg': require('../assets/images/aliments/papillote-de-saumon-aux-ravioles.jpg'),
  'saladeautomne.jpg': require('../assets/images/aliments/saladeautomne.jpg'),
  'steakpommesdeterresautes.jpg': require('../assets/images/aliments/steakpommesdeterresautes.jpg'),
  'oeuffeta.jpg': require('../assets/images/aliments/oeuffeta.jpg'),
  'escalopepouletetbrocolis.jpg': require('../assets/images/aliments/escalopepouletetbrocolis.jpg'),
  'soupecresson.jpg': require('../assets/images/aliments/soupecresson.jpg'),
  'auberginesfarcies.jpg': require('../assets/images/aliments/auberginesfarcies.jpg'),
  'salademle-automne.jpg': require('../assets/images/aliments/salademle-automne.jpg'),
  'croqueauchevrebacon.jpg': require('../assets/images/aliments/croqueauchevrebacon.jpg'),
  'gaspachoconcombrementhe.jpg': require('../assets/images/aliments/gaspachoconcombrementhe.jpg'),
  'padtha.jpg': require('../assets/images/aliments/padtha.jpg'),
  'saladelardonsoeufpoch.jpg': require('../assets/images/aliments/saladelardonsoeufpoch.jpg'),
  'epismaissteack.jpg': require('../assets/images/aliments/epismaissteack.jpg'),
  'pommesterresauteesavecailetlard.jpg': require('../assets/images/aliments/pommesterresauteesavecailetlard.jpg'),
  'pizzassgluten.jpg': require('../assets/images/aliments/pizzassgluten.jpg'),
  'ptesauxpetitspois.jpg': require('../assets/images/aliments/ptesauxpetitspois.jpg'),
  'gratinderavioles.jpg': require('../assets/images/aliments/gratinderavioles.jpg'),
  'avocatcitronvert.jpg': require('../assets/images/aliments/avocatcitronvert.jpg'),
  'cotelettesdagneausemoule.jpg': require('../assets/images/aliments/cotelettesdagneausemoule.jpg'),
  'pouletroti.jpg': require('../assets/images/aliments/pouletroti.jpg'),
  'spaghettiall-arrabiati.jpg': require('../assets/images/aliments/spaghettiall-arrabiati.jpg'),
  'omelette.jpg': require('../assets/images/aliments/omelette.jpg'),
  'soupepommedefenouil.jpg': require('../assets/images/aliments/soupepommedefenouil.jpg'),
  'maisaubeurreetsemoule.jpg': require('../assets/images/aliments/maisaubeurreetsemoule.jpg'),
  '4.jpg': require('../assets/images/aliments/4.jpg'),
  'saladelentilles.jpg': require('../assets/images/aliments/saladelentilles.jpg'),
  'tofusautlasiatique.jpg': require('../assets/images/aliments/tofusautlasiatique.jpg'),
  'courgettesvertesfarcies.jpg': require('../assets/images/aliments/courgettesvertesfarcies.jpg'),
  'saladequinoa,poischichebetterave.jpg': require('../assets/images/aliments/saladequinoa,poischichebetterave.jpg'),
  'saladequinoa,poischicheetbetterave.jpg': require('../assets/images/aliments/saladequinoa,poischicheetbetterave.jpg'),
  'saladetomatesetmozzarella.jpg': require('../assets/images/aliments/saladetomatesetmozzarella.jpg'),
  'pizza.jpg': require('../assets/images/aliments/pizza.jpg'),
  '7.jpg': require('../assets/images/aliments/7.jpg'),
  'tofubrouillasperges.jpg': require('../assets/images/aliments/tofubrouillasperges.jpg'),
  'jambonpuree.jpeg': require('../assets/images/aliments/jambonpuree.jpeg'),
  'gratindauphinois.jpg': require('../assets/images/aliments/gratindauphinois.jpg'),
  'gratindudauphin.jpg': require('../assets/images/aliments/gratindudauphin.jpg'),
  'saladelardonsetoeufpoch.jpg': require('../assets/images/aliments/saladelardonsetoeufpoch.jpg'),
  '9-croquemonsieur.jpg': require('../assets/images/aliments/9-croquemonsieur.jpg'),
  'oeufcoque.jpg': require('../assets/images/aliments/oeufcoque.jpg'),
  'feuilletfeta.jpg': require('../assets/images/aliments/feuilletfeta.jpg'),
  'rizaupoischiche.jpg': require('../assets/images/aliments/rizaupoischiche.jpg'),
  'img01.jpg': require('../assets/images/aliments/img01.jpg'),
  'quenelleslabechamel.png': require('../assets/images/aliments/quenelleslabechamel.png'),
  'gratinravioles.jpg': require('../assets/images/aliments/gratinravioles.jpg'),
  'rotideporcpommesdeterre.jpg': require('../assets/images/aliments/rotideporcpommesdeterre.jpg'),
  'oeufauplatfeta.jpg': require('../assets/images/aliments/oeufauplatfeta.jpg'),
  'ravioleslacrmeetaugruyre.jpg': require('../assets/images/aliments/ravioleslacrmeetaugruyre.jpg'),
  'pteaupesto.jpg': require('../assets/images/aliments/pteaupesto.jpg'),
  'saladepoischiche.jpg': require('../assets/images/aliments/saladepoischiche.jpg'),
  'tianlgumes.jpg': require('../assets/images/aliments/tianlgumes.jpg'),
  'croqueauchevreetchouxkhale.jpg': require('../assets/images/aliments/croqueauchevreetchouxkhale.jpg'),
  'oeufcocotteauxcpes.jpg': require('../assets/images/aliments/oeufcocotteauxcpes.jpg'),
  'gaspachoconcombrementhefraiche.jpg': require('../assets/images/aliments/gaspachoconcombrementhefraiche.jpg'),
  'boulettesdeviande.jpg': require('../assets/images/aliments/boulettesdeviande.jpg'),
  'soupedepoireaux.jpg': require('../assets/images/aliments/soupedepoireaux.jpg'),
};

const getAsciiName = name => name.replace(/[^\x00-\x7F]/g, '').replace(/\'/g, '').replace(/\s/g, '').toLowerCase();

const getImage = (name, meal) => {
  if (name == undefined) {return (meal) ? require('../assets/images/ui/meal_noimage.jpg') : require('../assets/images/ui/aliment_noimage.jpg');}

  const image = images[getAsciiName(name)];

  if (image == undefined) {return (meal) ? require('../assets/images/ui/meal_noimage.jpg') : require('../assets/images/ui/aliment_noimage.jpg');}
  return image;
};

export default {
  getAsciiName,
  getImage,
};

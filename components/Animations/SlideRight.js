import React, { Component } from 'react';
import * as Animatable from 'react-native-animatable';

class SlideRight extends Component {
  render() {
    return (
            <Animatable.View
                duration={500} 
                style={this.props.style}
                animation={{
                  from: { translateX: 1000 },
                  to: { translateX: 0 },   
                }}
            >
                { this.props.children }
            </Animatable.View>
    );
  }
}

export default SlideRight;

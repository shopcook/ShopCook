import React, { Component } from 'react';
import * as Animatable from 'react-native-animatable';

class SlideTop extends Component {
    render() {
        return (
            <Animatable.View
                duration={500} 
                style={this.props.style}
                animation={{
                    from: { translateY: 1000 },
                    to: { translateY: 0 },   
                }}
            >
                { this.props.children }
            </Animatable.View>
        );
    }
}

export default SlideTop;
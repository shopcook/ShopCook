import React from 'react';
import {
  Image,
  StyleSheet,
  TouchableHighlight,
  View,
  Dimensions,
} from 'react-native';

import { RobotoRegularText, RobotoCondensedRegularText, RobotoCondensedRegularInputText } from './StyledText';
import * as Colors from '../constants/Colors';

const _deviceIsSmall = () => {
  return Dimensions.get('window').width < 400;
}

class SimpleHorizontalCard extends React.Component {

  
  getCardTitle = (headerText) => {
    const { unit, input, inputChange, keyboardType } = this.props;

    if (input) {
      return (
        <React.Fragment>
          <RobotoCondensedRegularInputText
            keyboardType={keyboardType === undefined ? "default" : keyboardType}
            onChangeText={inputChange} underlineColorAndroid={"#fff"}
            style={[styles.cardTitle, styles.borderBottom]}
            returnKeyType="done"
          >
            {headerText}
          </RobotoCondensedRegularInputText>
          <RobotoCondensedRegularText 
            style={styles.cardTitle}
          >
            {unit !== undefined ? unit : ""}
          </RobotoCondensedRegularText>
        </React.Fragment> 
      )
    }
    return (
      <React.Fragment>
        <RobotoCondensedRegularText style={styles.cardTitle}>{headerText}</RobotoCondensedRegularText>
        <RobotoCondensedRegularText style={styles.cardTitle}>{unit !== undefined ? unit : ""}</RobotoCondensedRegularText>
      </React.Fragment>
    )
  }

  render() {
    const { bodyText, date, headerText, onPressLeft, onPressRight, disableActions } = this.props;
    const btnMoins = require('../assets/images/ui/btn_moins.png');
    const btnPlus = require('../assets/images/ui/btn_plus.png');
    const btnAvant = require('../assets/images/ui/btn_avant.png');
    const btnApres = require('../assets/images/ui/btn_apres.png');

    return (  
      <View style={[styles.cardImage, disableActions ? { width: '65%' } : null]}>
            <View>  
                <RobotoRegularText style={styles.cardBody}>{bodyText}</RobotoRegularText>
            </View>
            <View style={styles.cardBodyContainer}>
              <View style={[styles.titleContainer, disableActions ? { width: '100%', justifyContent: 'center'} : null]}> 
                { this.getCardTitle(headerText) }
              </View>
              {disableActions ? null : (
                  <View style={styles.cardActionContainer}>
                    <TouchableHighlight style={{borderRadius: 25}} activeOpacity={0.9} onPress={() => onPressLeft()}>
                      <Image  
                        source={date ? btnAvant : btnMoins}
                        style={[styles.cardAction]}
                      />
                    </TouchableHighlight>
                    <TouchableHighlight style={{borderRadius: 25, marginLeft: 8}} activeOpacity={0.9} onPress={() => onPressRight()}>
                      <Image
                        source={date ? btnApres : btnPlus}
                        style={styles.cardAction}
                      />
                    </TouchableHighlight>
                  </View>
              )}
            </View>
      </View>
    );
  }
} 

const styles = StyleSheet.create({
  cardImage: {
    justifyContent: 'space-around', 
    alignItems: 'center', 
    flexDirection: 'column',
    padding: _deviceIsSmall() ? 10 : 20, 
    width: '80%',
    margin: 8,
    borderWidth: 2,
    borderColor: '#6cc8be',
    borderRadius: 8,
  },
  cardBodyContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row', 
  },
  titleContainer: {
    flexDirection: 'row',
    paddingRight: 15,
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '45%'

  },
  cardActionContainer: {
    flexDirection: 'row',
    alignItems: 'center', 
    justifyContent: 'flex-start',
    width: '55%'

  },
  cardAction: {
    width: _deviceIsSmall() ? 35 : 50,
    height: _deviceIsSmall() ? 35 : 50,
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.default.framboise,
  },
  cardTitle: {
    fontSize: _deviceIsSmall() ? 25 : 35,
    color: Colors.default.framboise
  },
  cardBody: {
    fontSize: _deviceIsSmall() ? 15 : 20,
    paddingBottom: 10,  
    color: Colors.default.darkGrey
  },
});


export default SimpleHorizontalCard;


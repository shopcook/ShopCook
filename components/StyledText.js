import React from 'react';
import { Text, TextInput } from 'react-native';

export class MonoText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'space-mono' }]} />;
  }
}

export class ToolboxHunterswoodText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'toolbox-wood-regular'}]} />;
  }
}

export class RobotoRegularText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'roboto-regular'}]} />;
  }
}

export class RobotoBoldText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'roboto-regular', fontWeight: 'bold'}]} />;
  }
}

export class RobotoCondensedRegularInputText extends React.Component {
  render() {
    return <TextInput {...this.props} style={[this.props.style, { fontFamily: 'roboto-condensed-regular'}]} />;
  }
}

export class RobotoCondensedRegularText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'roboto-condensed-regular'}]} />;
  }
}

export class RobotoMediumText extends React.Component {
  render() {
    return <Text {...this.props} style={[this.props.style, { fontFamily: 'roboto-medium'}]} />;
  }
}

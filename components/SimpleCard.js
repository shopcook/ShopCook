import React from 'react';
import {
  Image,
  Dimensions,
  StyleSheet,
  TouchableHighlight,
  View
} from 'react-native';

import { RobotoRegularText, RobotoCondensedRegularText, RobotoCondensedRegularInputText } from './StyledText';
import * as Colors from '../constants/Colors';

const _deviceIsSmall = () => {
  return Dimensions.get('window').width < 400;
}

class SimpleCard extends React.Component {

  
  getCardTitle = (headerText) => {
    const { unit, input, inputChange, keyboardType } = this.props;

    if (input) {
      return (
        <React.Fragment>
          <RobotoCondensedRegularInputText
            keyboardType={keyboardType === undefined ? "default" : keyboardType}
            onChangeText={inputChange} underlineColorAndroid={"#fff"}
            style={styles.cardTitle}
            returnKeyType="done"
          >
            {headerText}
          </RobotoCondensedRegularInputText>
          <RobotoCondensedRegularText 
            style={styles.cardTitle}
          >
            {unit !== undefined ? unit : ""}
          </RobotoCondensedRegularText>
        </React.Fragment> 
      )
    }
    return (
      <React.Fragment>
        <RobotoCondensedRegularText style={styles.cardTitle}>{headerText}</RobotoCondensedRegularText>
        <RobotoCondensedRegularText style={styles.cardTitle}>{unit !== undefined ? unit : ""}</RobotoCondensedRegularText>
      </React.Fragment>
    )
  }

  render() {
    const { bodyText, headerText, onPressLeft, onPressRight, disableActions, cardHTML } = this.props;

    const cardStyle = cardHTML || "cardImage";

    return (  
      <View style={styles[cardStyle]}>
      {cardHTML ? null :  <Image style={styles.card} source={require('../assets/images/ui/cadre.png')} /> }
            <View style={{flexDirection: 'row'}}> 
              { this.getCardTitle(headerText) }
            </View> 
            <RobotoRegularText style={styles.cardBody}>{bodyText}</RobotoRegularText>
            {disableActions ? null : (
                <View style={styles.cardActionContainer}>
                  <TouchableHighlight style={{borderRadius: 100}} activeOpacity={0.9} onPress={() => onPressLeft()}>
                    <Image
                      source={require('../assets/images/ui/btn_moins.png')}
                      style={styles.cardAction}
                    />
                  </TouchableHighlight>
                  <TouchableHighlight style={{borderRadius: 100}} activeOpacity={0.9} onPress={() => onPressRight()}>
                    <Image
                      source={require('../assets/images/ui/btn_plus.png')}
                      style={styles.cardAction}
                    />
                  </TouchableHighlight>
                </View>
            )}
      </View>
    );
  }
} 

const styles = StyleSheet.create({
  cardImage: {
    justifyContent: 'center', 
    alignItems: 'center',
    flexDirection: 'column',
    padding: 8,
    width: '50%',
 
  },
  card : {
    position: 'absolute',
    resizeMode: 'contain',
    width: '90%' 
  }, 
  cardHTML: { // When react-native border animation will be fixed
    borderWidth: 2,
    borderColor: '#6cc8be',
    zIndex: 1000,  
    borderRadius: 8,
    width: '35%',
    paddingTop: _deviceIsSmall() ? 0 : 20, 
    paddingBottom: _deviceIsSmall() ? 0 : 20, 
    margin: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  cardActionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 5,
    width: '100%',
  },
  cardAction: {
    width: _deviceIsSmall() ? 40 : 50, 
    height: _deviceIsSmall() ? 40 : 50, 
  },
  cardTitle: {
    fontSize: _deviceIsSmall() ? 30 : 40, 
    color: Colors.default.framboise
  },
  cardBody: {
    fontSize: 20,
    color: Colors.default.darkGrey
  },
});


export default SimpleCard;


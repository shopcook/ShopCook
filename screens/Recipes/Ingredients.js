import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import HeaderScreen from '../../components/HeaderScreen';
import * as Colors from '../../constants/Colors';
import { RobotoBoldText, RobotoRegularText } from '../../components/StyledText';
import { fetchRecipe } from '../../actions';
import Images from '../../constants/Images';

const _deviceIsSmall = () => {
    return Dimensions.get('window').width < 400;
}

class IngredientsScreen extends Component {
    constructor(props) {
        super(props);

        const { dispatch, recipeToDisplay } = props;

        dispatch(fetchRecipe(recipeToDisplay.recipeId));
    }

    render() {
        const { recipeToDisplay, recipes } = this.props;
        const index = recipes.findIndex(recipe => recipe.id == recipeToDisplay.recipeId);
        const ingredients = (index === -1) ? [] : recipes[index].ingredients; 
        const name = (index === -1 || !recipes[index].infos) ? "Loading..." : recipes[index].infos.name;

        return (
            <View style={styles.container}>
                <HeaderScreen name="Ingrédients" previous="MealScreen" color={Colors.default.darkGrey} backgroundColor={true} />
                <RobotoBoldText style={styles.title}>{name}</RobotoBoldText>
                <ScrollView> 
                    <View style={[styles.row, {paddingBottom: 8, justifyContent: 'flex-end'}]}>
                        <View style={{paddingRight: 30}}>  
                            <RobotoBoldText style={styles.ingredientName} >Origine</RobotoBoldText>   
                        </View>
                    </View>
                    {ingredients.map((ingredient, index) => (
                        <React.Fragment key={index}> 
                            <View style={styles.dviderStyle} />
                            <View style={styles.row}>
                                <View style={{flexDirection: 'row', paddingLeft: 20, alignItems: 'center'}}>
                                    <View style={styles.ingredientPreviewContainer}>
                                        <Image style={styles.circleImage} source={Images.getImage(ingredient.image, false)} />
                                    </View>
                                    <RobotoRegularText style={styles.ingredientName} >{ingredient.name}</RobotoRegularText>
                                </View>
                                <View style={{paddingRight: 25}}>
                                    <RobotoRegularText style={styles.ingredientName} >Inconnue</RobotoRegularText>   
                                </View>
                            </View>
                        </React.Fragment>
                    ))}
                    <View style={styles.dviderStyle} />
                </ScrollView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: 'white',
    },
    title: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 25,
        textAlign: 'center',  
        color: Colors.default.framboise,
        padding: 12,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 1
    },
    ingredientPreviewContainer: {
        padding: 8,
    },
    circle: {
        height: 40,
        width: 40,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: Colors.default.framboise,
        backgroundColor: Colors.default.darkGrey
    },
    circleImage: {
        height: 40,
        width: 40,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: Colors.default.framboise,
    },
    ingredientName: {
        paddingLeft: _deviceIsSmall() ? 4 : 16,
        fontSize: _deviceIsSmall() ? 12 : 15,
        color: Colors.default.darkGrey
    },
    dviderStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        height: StyleSheet.hairlineWidth,
      }
  });
  

function mapStateToProps({ recipeToDisplay, recipes }) {
    return {
        recipeToDisplay,
        recipes,
    }
  }
  
  export default connect(mapStateToProps)(IngredientsScreen);
  
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { WebBrowser } from 'expo';

import * as Colors from '../../constants/Colors';
import { RobotoBoldText } from '../../components/StyledText';
import Touchable from '../../components/Touchable';
import { updateAllergiesPreferences } from '../../actions';

class Compliency extends React.Component {
  render() {
    const { index, name, active, dispatch, image } = this.props;

    const backgroundColor = active ? Colors.default.whiteGrey : 'white';
    return (
      <Touchable onPress={() => {dispatch(updateAllergiesPreferences(index, name, !active))}}>
      <View style={[styles.container, { backgroundColor }]}>
        <View style={{position: 'relative'}}>
          <Image
            source={image}
            style={styles.alimentImage}
          />
          {
            active ? (
              <Image
                source={require('../../assets/images/ui/photo_ingrédients_trouvé_3.png')}
                style={[styles.alimentImageValidate, {position: 'absolute'}]}
              />
            ) : null
          }
        </View>
        <RobotoBoldText style={styles.name}>{name}</RobotoBoldText>
      </View>
      </Touchable>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingLeft: '10%',
    alignItems: 'center',
    paddingBottom: 8,
    paddingTop: 8,
  },
  name: {
    fontSize: 18,
    color: Colors.default.darkGrey,
  },
  alimentImage: {
    width: 50,
    height: 50,
    marginRight: 16,
    borderRadius: 25
  },
 alimentImageValidate: {
    width: 50,
    height: 50,
    marginRight: 16,
    borderColor: Colors.default.framboise,
    borderWidth: 2,
    borderRadius: 25
 }
});


function mapStateToProps() {
  return {

  }
}

export default connect(mapStateToProps)(Compliency);

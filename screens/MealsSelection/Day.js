import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { MaterialIcons } from '@expo/vector-icons';
import moment from 'moment';
import * as Animatable from 'react-native-animatable'; 
import * as Colors from '../../constants/Colors';
import { RobotoBoldText } from '../../components/StyledText';

import { updateDays, updateCurrentScreen, updateMealsSelected } from '../../actions';
import { Days } from '../../constants/Days';

import HeaderScreen from '../../components/HeaderScreen';
import SimpleCard from '../../components/SimpleCard';
import { ButtonAction } from '../../components/Button';

const _deviceIsSmall = () => {
  return Dimensions.get('window').width < 420;
}

class DayScreen extends React.Component {
  constructor(props) {
    super(props);

    const { preferences } = props;
    this.state = {
      lunchAdults: preferences.defaultValues.adults,
      dinerAdults: preferences.defaultValues.adults,
      lunchChildren: preferences.defaultValues.children,
      dinerChildren: preferences.defaultValues.children,
    }
  }

  getNbDaysBetweenBegin = () => {
    const { budget } = this.props;
    const budgetDay = moment(budget.from).isoWeekday() - 1;
    const beginDay = Days.indexOf(budget.shoppingDay);

    if (budgetDay <= beginDay)
      return moment(budget.from).add(beginDay - budgetDay, 'days');
    return moment(budget.from).add(7 - budgetDay + beginDay, 'days');
  }

  getNbDaysRemaining = () => {
    const { budget, days } = this.props;
    return budget.nbDays - days.length;
  }

  getCurrentDate = () => {
    const { budget, days } = this.props;
    const date = moment(this.getNbDaysBetweenBegin()).add(days.length, 'days');

    return `${Days[date.isoWeekday() - 1]} ${date.format("DD")}`;
  }

  handleViewRef = ref => this.view = ref;

  _nextStep = () => {
    const { dispatch, days } = this.props;

    if (days.length === 0)
      dispatch(updateMealsSelected([]));

    dispatch(updateDays([...days, { 
      day: this.getCurrentDate(),
      lunch: { adults: parseInt(this.state.lunchAdults), children: parseInt(this.state.lunchChildren) },
      diner: { adults: parseInt(this.state.dinerAdults), children: parseInt(this.state.dinerChildren) },
    }]));

    if (this.getNbDaysRemaining() <= 1) {
      dispatch(updateCurrentScreen("MealScreen"));
    } else {
      const view = this.view;
      this.view.fadeOutLeftBig(200).then(() => view.fadeInRightBig(200));      
    }
  }

  render() {
    const { dinerChildren, lunchAdults, dinerAdults, lunchChildren } = this.state;

    return (
      <Animatable.View
                duration={500} 
                style={styles.container}
                animation={{
                    from: { translateX: 1000 },
                    to: { translateX: 0 },   
                }}
                ref={this.handleViewRef}
            >
        <HeaderScreen name={this.getCurrentDate()} color="white" />
        <View style={styles.mainView}>  
        <View style={[styles.item, styles.separation]}>
          <RobotoBoldText style={styles.title}>Midi</RobotoBoldText>
          <View style={styles.cardContainer}>
            <SimpleCard
              bodyText="Enfant(s)"
              headerText={lunchChildren}
              input={true}
              keyboardType="numeric"
              inputChange={(lunchChildren) => parseInt(lunchChildren) > 0 && this.setState({ lunchChildren: parseInt(lunchChildren) })}
              onPressRight={() => this.setState({ lunchChildren: lunchChildren + 1 })}
              onPressLeft={() => lunchChildren > 0 && this.setState({ lunchChildren: lunchChildren - 1 })}
            />
            <SimpleCard
              bodyText="Adulte(s)"
              headerText={lunchAdults}
              input={true}
              keyboardType="numeric"
              inputChange={(lunchAdults) => parseInt(lunchAdults) > 0 && this.setState({  lunchAdults: parseInt(lunchAdults) })}
              onPressRight={() => this.setState({ lunchAdults: lunchAdults + 1 })}
              onPressLeft={() => lunchAdults > 0 && this.setState({ lunchAdults: lunchAdults - 1 })}
            />
          </View>
        </View> 
        <View style={[styles.item, styles.separation]}>
          <RobotoBoldText style={styles.title}>Soir</RobotoBoldText>
          <View style={styles.cardContainer}>
            <SimpleCard
              bodyText="Enfant(s)"
              headerText={dinerChildren}
              input={true}
              keyboardType="numeric"
              inputChange={(dinerChildren) => parseInt(dinerChildren) > 0 && this.setState({ dinerChildren: parseInt(dinerChildren) })}
              onPressRight={() => this.setState({ dinerChildren: dinerChildren + 1 })}
              onPressLeft={() => dinerChildren > 0 && this.setState({ dinerChildren: dinerChildren - 1 })}
            />
            <SimpleCard
              bodyText="Adulte(s)"
              headerText={dinerAdults}
              input={true}
              keyboardType="numeric"
              inputChange={(dinerAdults) => parseInt(dinerAdults) > 0 && this.setState({ dinerAdults: parseInt(dinerAdults) })}
              onPressRight={() => this.setState({ dinerAdults: dinerAdults + 1 })}
              onPressLeft={() => dinerAdults > 0 && this.setState({ dinerAdults: dinerAdults - 1 })}
            />
          </View>
        </View>
        <ButtonAction name={(this.getNbDaysRemaining() <= 1) ? "Générer les repas" : "Jour suivant"} onAction={this._nextStep}>
          <MaterialIcons name="play-circle-outline" size={35} color="white" />
        </ButtonAction>
        </View>
      </Animatable.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  mainView: {
    backgroundColor: 'white',
    height: '100%',
  },
  separation: {
    marginBottom: _deviceIsSmall() ? 10 : 20
  },
  title: {
    fontSize: 20,
    color: Colors.default.darkGrey,
    paddingBottom: 20,
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: _deviceIsSmall() ? 10 : 20,
  },
  item: {
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 10
  },
});


function mapStateToProps({
  budget,
  days,
  preferences,
}) {
  return {
    budget,
    days,
    preferences,
  }
}

export default connect(mapStateToProps)(DayScreen);

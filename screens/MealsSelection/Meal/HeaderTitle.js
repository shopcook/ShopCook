import React from 'react';
import {
  Image,
  ImageBackground,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Button,
  View
} from 'react-native';
import { connect } from 'react-redux';
import { WebBrowser } from 'expo';
import { FontAwesome } from '@expo/vector-icons';
import { ToolboxHunterswoodText, RobotoBoldText, RobotoRegularText } from '../../../components/StyledText';

class HeaderTitle extends React.Component {
  render() {
    const { day, nbPeople, mealType } = this.props;

    return (
      <View style={[styles.flexRow, styles.container]}>
        <View style={styles.flexColumn}>
          <ToolboxHunterswoodText style={styles.day}>{day}</ToolboxHunterswoodText>
          <RobotoRegularText style={styles.mealType}>{mealType}</RobotoRegularText>
        </View>
        <Image
          source={require('../../../assets/images/ui/ligne.png')}
          style={styles.imageSeparation}
        />
        <View style={styles.flexColumn}>
          <FontAwesome style={{padding: 4}} name="user-o" size={25} color="white"/>
          <RobotoBoldText style={styles.nbPeople}>{nbPeople}</RobotoBoldText>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15
  },
  flexRow: {
    flexDirection: 'row',
  },
  flexColumn: {
    flexDirection: 'column',
  },
  imageSeparation: {
    width: 20,
    height: 60,
    resizeMode: 'contain'
  },
  day: {
    fontSize: 35,
    color: 'white',
  },
  mealType: {
    fontSize: 20,
    color: 'white',
    textAlign: 'right'
  },
  nbPeople: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center'
  }
});


function mapStateToProps({
  currentScreen,
}) {
  return {
    currentScreen,
  }
}

export default connect(mapStateToProps)(HeaderTitle);


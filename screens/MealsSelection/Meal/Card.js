import React from 'react';
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Feather } from '@expo/vector-icons';

import * as Colors from '../../../constants/Colors';
import { RobotoBoldText, RobotoRegularText } from '../../../components/StyledText';

import Stars from '../../../components/Stars';
import Touchable from '../../../components/Touchable';
import { updateRecipeToDisplay } from '../../../actions';

import Images from '../../../constants/Images';

const _deviceIsSmall = () => {
  return Dimensions.get('window').width < 400;
}

class Card extends React.Component {

  convertMinsToHrsMins(mins) {
    let h = Math.floor(mins / 60);
    let m = mins % 60;
    h = h < 10 ? h : h;
    m = m < 10 ? '0' + m : m;
    return `${h}h${m}`;
  }

  render() {
    const { card, dispatch, swipeLeft, swipeRight } = this.props;
    if (card === undefined)
      return null;  
    return (
      <View style={styles.card}>
        <RobotoBoldText style={styles.cardTitle} >{card.name.toUpperCase()}</RobotoBoldText>
        <View style={[styles.cardSubtitleContainer, {paddingBottom: 8}]}> 
          <RobotoBoldText style={styles.cardFooterTitle}>Note globale: </RobotoBoldText>
          <Stars nbStars={5} mealId={card.id} /> 
        </View>
        <View style={styles.cardSubtitleContainer}>
          <Feather style={styles.cardSubtitleIcon} name="clock" size={20} color={Colors.default.darkGrey} />
          <RobotoBoldText style={styles.cardSubtitleText}>{this.convertMinsToHrsMins(parseInt(card.duration))}</RobotoBoldText>
        </View> 
        <View style={styles.cardImageContainer}>
          <Image source={Images.getImage(card.image, true)} style={styles.circleImage} />
        </View>
        <View style={styles.cardSubtitleContainer}>
            <TouchableOpacity style={styles.cardSubtitleContainer} onPress={() => dispatch(updateRecipeToDisplay(card.id, "MealScreen", card.index))}>
              <Image 
                source={require('../../../assets/images/ui/ingredients.png')}
                style={styles.location} 
              /> 
              <RobotoBoldText textAlign="left" style={[styles.cardSubtitleText, {paddingLeft: 4}]}>Origine des{"\n"}ingrédients</RobotoBoldText>
            </TouchableOpacity>
        </View> 
        <View style={styles.cardActionsContainer}>
          <View style={{justifyContent: 'center', flexDirection: 'column', alignItems: 'center'}}>
            <Touchable underlayColor="#fff" onPress={swipeLeft}>
              <Image
                source={require('../../../assets/images/ui/picto_rejeter.png')}
                style={styles.cardAction}
              />
            </Touchable> 
            <RobotoRegularText style={styles.cardActionLeft}>Pas cette fois...</RobotoRegularText>
          </View>
          <View  style={{justifyContent: 'center', flexDirection: 'column', alignItems: 'center'}}>
            <Touchable underlayColor="#fff" onPress={swipeRight}> 
            <Image
                source={require('../../../assets/images/ui/picto_garder.png')}
                style={styles.cardAction}
              />
            </Touchable>
            <RobotoRegularText style={styles.cardActionRight}>Oh oui!</RobotoRegularText>
          </View>
        </View> 
      </View> 
    );
  }
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "white",
    flexDirection: 'column'
  }, 
  cardTitle: { 
    textAlign: "center",
    fontSize: _deviceIsSmall() ? 22 : 28,
    backgroundColor: "transparent",
    color: Colors.default.framboise,
    fontWeight: 'bold',
    padding: 8,
  },
  cardSubtitleIcon: {
    paddingRight: 4
  },
  cardSubtitleText: {
    color: Colors.default.darkGrey,
    fontSize: 18
  },
  cardSubtitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  cardImageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16
  },
  cardFooter: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  cardFooterTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    color: Colors.default.darkGrey,
    fontSize: 18
  },
  cardActionsContainer: {
    justifyContent: 'space-around', 
    padding: _deviceIsSmall() ? 10 : 20,
    alignItems: 'center',
    flexDirection: 'row'
  },
  cardAction: {
    width: _deviceIsSmall() ? 60 : 80,
    height: _deviceIsSmall() ? 60 : 80,
    resizeMode: 'contain' 
  },
  location: {
    width: 40,
    height: 40,
    resizeMode: 'contain' 
  },
  cardActionLeft: {
    fontSize: 15,
    color: Colors.default.blueGreen
  },
  cardActionRight: {
    fontSize: 15,
    color: Colors.default.framboise
  },
  circleImage: {  
    width: _deviceIsSmall() ? 160 : 200,
    height: _deviceIsSmall() ? 160 : 200,
    borderRadius: _deviceIsSmall() ? 80 : 100,
    borderWidth: 2,
    borderColor: Colors.default.blueGreen,
  }, 
  circle: {
    width: _deviceIsSmall() ? 160 : 200,
    height: _deviceIsSmall() ? 160 : 200,
    borderRadius: _deviceIsSmall() ? 80 : 100,
    backgroundColor: Colors.default.whiteGrey,
    borderColor: Colors.default.blueGreen,
  }
});


function mapStateToProps({
  budget,
  days,
  preferences,
}) {
  return {
    budget,
    days,
    preferences,
  }
}

export default connect(mapStateToProps)(Card);

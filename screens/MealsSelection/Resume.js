import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { MaterialIcons } from '@expo/vector-icons';

import * as Colors from '../../constants/Colors';

import { updateCurrentScreen } from '../../actions';
import { Screens } from '../../store';

import HeaderScreen from '../../components/HeaderScreen';
import SimpleCard from '../../components/SimpleCard';
import { ButtonAction } from '../../components/Button';
import SlideRight from '../../components/Animations/SlideRight';
import SimpleHorizontalCard from '../../components/SimpleHorizontalCard';

class ResumeSelectionScreen extends React.Component {

  render() {
    const { budget, mealsSelected, dispatch } = this.props;

    const price = mealsSelected.filter(meal => !meal.empty).reduce((a, b) => parseFloat(a) + parseFloat(b.price), 0.0);

    return (
      <SlideRight style={styles.container}>
        <HeaderScreen 
          subtitle="Prix calculé sur la base d'une portion"
          colorSubtitle="white"
          fontSizeSubtitle={20}
          name="Estimation du panier"
          fontSize={30}
          color="white"
        />
        <View style={styles.mainView} >
        <View style={styles.cardContainer}>
          <SimpleHorizontalCard
            bodyText="Mon budget repas"
            unit={"€"}
            headerText={budget.value}
            disableActions={true}
          />
          <SimpleHorizontalCard
            bodyText="Coûts de ma sélection"
            unit={"€"}
            headerText={Math.floor(price)}
            disableActions={true}
          />
          <SimpleHorizontalCard
            bodyText="Economies réalisées"
            unit={"€"}
            headerText={budget.value - Math.floor(price)}
            disableActions={true}
          />
        </View>
        <ButtonAction name="Terminer" onAction={() => dispatch(updateCurrentScreen(Screens.RecipesScreen))}>
          <MaterialIcons name="play-circle-outline" size={35} color="white" />
        </ButtonAction>
        </View>
      </SlideRight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  mainView: {
    backgroundColor: 'white',
    height: '100%',
  },
  cardContainer: {
    flexDirection: 'column',
    justifyContent: 'center', 
    alignItems: 'center',
    flexWrap: 'wrap',
    marginTop: 20,
    marginBottom: 20
  },
});


function mapStateToProps({
  budget,
  mealsSelected,
}) {
  return {
    budget,
    mealsSelected,
  }
}

export default connect(mapStateToProps)(ResumeSelectionScreen);

export function updateCurrentScreen(currentScreen) {
  return {
    type: 'UPDATE_CURRENT_SCREEN',
    payload: {
      currentScreen,
    }
  }
}

export function updateOnload(onLoad) {
  return {
    type: 'UPDATE_ON_LOAD',
    payload: {
      onLoad,
    }
  }
}

export function updateShoppingListy(shoppingList) {
  return {
    type: 'UPDATE_SHOPPING_LIST',
    payload: {
      shoppingList,
    }
  }
}

export function clearMeals() {
  return {
    type: 'CLEAR_MEALS',
    payload: {
    },
  }
}

export function addMeal() {
  return {
    type: 'ADD_MEAL',
    payload: {
    },
  }
}

export function updateStars(stars) {
  return {
    type: 'UPDATE_STARS',
    payload: {
      stars,
    }
  }
}

export function updateRecipeToDisplay(recipeId, previousScreen, currentMealIndex) {
  return {
    type: 'UPDATE_RECIPE_TO_DISPLAY',
    payload: {
      recipeId,
      previousScreen,
      currentMealIndex,
    },
  }
}

export function updateFullRecipeToDisplay(recipeId, nbPeople, nbPeopleCoefficient) {
  return {
    type: 'UPDATE_FULL_RECIPE_TO_DISPLAY',
    payload: {
      recipeId,
      nbPeople,
      nbPeopleCoefficient,
    },
  }
}

export function updateMeals(meals) {
  return {
    type: 'UPDATE_MEALS',
    payload: {
      meals,
    }
  }
}

export function updateMealsSelected(mealsSelected) {
  return {
    type: 'UPDATE_MEALS_SELECTED',
    payload: {
      mealsSelected,
    }
  }
}

export function previousScreen() {
  return {
    type: 'PREVIOUS_SCREEN',
    payload: {}
  }
}

export function updateDays(days) {
  return {
    type: 'UPDATE_DAYS',
    payload: {
      days,
    }
  }
}

export function updateAllergiesPreferences(index, text, active) {
  return {
    type: 'UPDATE_PREFERENCES_ALLERGIES',
    payload: {
      index,
      allergy: {
        active,
      },
    }
  }
}

export function updateBudgetValue(value) {
  return {
    type: 'UPDATE_BUDGET_VALUE',
    payload: {
      value,
    }
  }
}

export function updateBudgetNbDays(nbDays) {
  return {
    type: 'UPDATE_BUDGET_NB_DAYS',
    payload: {
      nbDays,
    }
  }
}

export function updateBudgetDate(dateFrom) {
  return {
    type: 'UPDATE_BUDGET_FROM',
    payload: {
      from: dateFrom,
    }
  }
}

export function updateBudgetShoppingDay(shoppingDay) {
  return {
    type: 'UPDATE_BUDGET_SHOPPING_DAY',
    payload: {
      shoppingDay,
    }
  }
}

export function fetchRecipe(id) {
  return {
    types: ['REQUEST_LOAD','REQUEST_SUCCESS','REQUEST_ERROR'],
    payload: {
      request: {
        method: 'get',
        url: `/recipe/recipe.php?id=${id}`,
      }
    }
  }
}

export function fetchRecipes(ids) {
  return {
    types: ['REQUEST_LOAD','REQUEST_SUCCESS','REQUEST_ERROR'],
    payload: {
      request: {
        method: 'get',
        url: `/recipe/recipe.php?id=${ids.join(",")}`,
      }
    }
  }
}

export function fetchRecipeSelection(data) {
  return {
    types: ['REQUEST_LOAD','REQUEST_SUCCESS','REQUEST_ERROR'],
    payload: {
      request: {
        method: 'post',
        url: '/recipe/selection.php',
        data: data,
      }
    }
  }
}

export function updateAdultsPreferences(adults) {
  return {
    type: 'UPDATE_PREFERENCES_ADULTS',
    payload: {
      adults,
    }
  }
}

export function updateChildrenPreferences(children) {
  return {
    type: 'UPDATE_PREFERENCES_CHILDREN',
    payload: {
      children,
    }
  }
}
import React from 'react';
import PropTypes from 'prop-types';
import { StatusBar, StyleSheet, View, ImageBackground } from 'react-native';
import { AppLoading, Asset, Font, Icon } from 'expo';
import { Provider } from 'react-redux';
import { store } from './store';
import AppNavigator from './navigation/AppNavigator';

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };
  
  render() {
    console.disableYellowBox = true; // eslint-disable-line

    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <Provider store={store}>
          <View style={styles.container}>
              <StatusBar barStyle="default" hidden={true} />
              <AppNavigator/>
          </View>
      </Provider>
      );
    }
  }

  _loadResourcesAsync = async () => Promise.all([
    Asset.loadAsync([
      require('./assets/images/splash.png'),
      require('./assets/images/ui/fond.png'),
      require('./assets/images/ui/logo.png'),
      require('./assets/images/ui/btn_1.png'),
      require('./assets/images/ui/btn_2.png'),
      require('./assets/images/ui/btn_back.png'),
      require('./assets/images/ui/btn_plus.png'),
      require('./assets/images/ui/btn_moins.png'),
      require('./assets/images/ui/btn_apres.png'),
      require('./assets/images/ui/btn_avant.png'),
      require('./assets/images/ui/photo_ingrédients_trouvé_1.png'),
      require('./assets/images/ui/ligne.png'),
      require('./assets/images/ui/photo_ingrédients_trouvé_3.png'),
      require('./assets/images/ui/photo_ingrédients_1.png'),
      require('./assets/images/ui/photo_ingrédients_trouvé_3.png'),
      require('./assets/images/ui/photo_ingrédients_2.png'),
      require('./assets/images/ui/cadre.png'),
      require('./assets/images/ui/picto_coeur.png'),
      require('./assets/images/ui/picto_croix.png'),
      require('./assets/images/ui/fond_trans.png'),
      require('./assets/images/ui/picto_poubelle.png'),
      require('./assets/images/ui/aliment_noimage.jpg'),
      require('./assets/images/ui/gluten.jpg'),
      require('./assets/images/ui/lactose.jpg'),
      require('./assets/images/ui/meal_noimage.jpg'),
      require('./assets/images/ui/stuff.jpg'),
      require('./assets/images/ui/vegan.jpg'),
      require('./assets/images/ui/vegetarien.jpg'),
      require('./assets/images/ui/sportif.jpg'),
      require('./assets/images/ui/healthy.jpg'),
    ]),
    Font.loadAsync({
      ...Icon.Ionicons.font,
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      'roboto-bold': require('./assets/fonts/Roboto-Bold.ttf'),
      'roboto-medium': require('./assets/fonts/Roboto-Medium.ttf'),
      'roboto-regular': require('./assets/fonts/Roboto-Regular.ttf'),
      'roboto-condensed-bold': require('./assets/fonts/RobotoCondensed-Bold.ttf'),
      'roboto-condensed-regular': require('./assets/fonts/RobotoCondensed-Regular.ttf'),
      'toolbox-wood-regular': require('./assets/fonts/Toolbox_Hunterswood-Regular.otf'),
    }),
  ]);

  _handleLoadingError = (error) => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error); // eslint-disable-line
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

App.propTypes = {
  skipLoadingScreen: PropTypes.bool,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});

